package com.progressoft.application;

import com.progressoft.application.producer.FileProducer;
import com.progressoft.application.producer.TransactionProducer;
import com.progressoft.application.writer.FileMatchingTransactionWriter;
import com.progressoft.application.writer.FileMissedTransactionWriter;
import com.progressoft.domain.Transaction;
import com.progressoft.domain.TransactionComparison;
import com.progressoft.domain.TransactionOutput;
import com.progressoft.domain.exception.FileReaderException;
import com.progressoft.domain.reader.TransactionImporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Map;
import java.util.Scanner;

public class MainApplication {

    private static final Logger logger = LoggerFactory.getLogger(MainApplication.class);
    private static final String OUTPUT_PATH = "reconciliation-results";
    private static final FileMissedTransactionWriter fileMisMatchTransactionWriter =
            new FileMissedTransactionWriter();
    private static final FileMatchingTransactionWriter fileMatchedTransactionWriter =
            new FileMatchingTransactionWriter();
    private static final FileMissedTransactionWriter fileMissedTransactionWriter =
            new FileMissedTransactionWriter();

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        logger.info("Enter source file location");
        String sourceFile = scanner.nextLine();
        logger.info("Enter source file format");
        String sourceFormat = scanner.nextLine();
        logger.info("Enter target file location");
        String targetFile = scanner.nextLine();
        logger.info("Enter target file format");
        String targetFormat = scanner.nextLine();
        startReconciliation(sourceFile, sourceFormat, targetFile, targetFormat);
    }

    private static void startReconciliation(String sourceFile, String sourceFormat, String targetFile, String targetFormat) throws FileReaderException {

            Map<String, Transaction> sourceTransaction = TransactionImporter.readFile(sourceFormat, sourceFile);
            Map<String, Transaction> targetTransaction = TransactionImporter.readFile(targetFormat, targetFile);

            TransactionComparison transactionComparison =
                    new TransactionComparison(sourceFile, targetFile, sourceTransaction, targetTransaction);
            TransactionOutput transactionOutput = transactionComparison.compareSourceAndTarget();

            TransactionProducer producer = new FileProducer(fileMissedTransactionWriter,
                    fileMisMatchTransactionWriter,
                    fileMatchedTransactionWriter);
            String result = producer.produce(OUTPUT_PATH + File.separator, transactionOutput);
            logger.info(result);
    }
}
