package com.progressoft.application.writer;


import com.progressoft.domain.Transaction;
import com.progressoft.domain.converter.AmountConverter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class FileMatchingTransactionWriter extends FileTransactionWriter {

    private static final String FILE_HEADER = "transaction id,amount,currecny code,value date";

    @Override
    void writeContent(List<Transaction> transactions, FileWriter fileWriter) throws IOException {
        fileWriter.append(FILE_HEADER);
        fileWriter.append(NEW_LINE_SEPARATOR);

        for (Transaction transaction : transactions) {
            fileWriter.append(transaction.getId());
            fileWriter.append(COMMA_DELIMITER);
            String amount = AmountConverter.getAmount(transaction.getAmount(), transaction.getCurrencyCode());
            fileWriter.append(amount);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(String.valueOf(transaction.getCurrencyCode()));
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(String.valueOf(transaction.getTransactionDate()));
            fileWriter.append(NEW_LINE_SEPARATOR);
        }

    }


}