package com.progressoft.application.writer;

import com.progressoft.domain.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

abstract class FileTransactionWriter {
    private static final Logger logger = LoggerFactory.getLogger(FileTransactionWriter.class);
    public static final String COMMA_DELIMITER = ",";
    public static final String NEW_LINE_SEPARATOR = "\n";

    public void writeTransactions(String path, List<Transaction> transactions) {
        File file = new File(path);
        file.getParentFile().mkdir();
        try (FileWriter fileWriter = new FileWriter(file)) {
            writeContent(transactions, fileWriter);
        } catch (IOException e) {
            logger.info("Error while writing the file ");
        }
    }

    abstract void writeContent(List<Transaction> transactions, FileWriter fileWriter) throws IOException;

}
