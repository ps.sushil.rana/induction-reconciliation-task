package com.progressoft.application.producer;

import com.progressoft.domain.TransactionOutput;

public interface TransactionProducer {

    String produce(String path, TransactionOutput output);

}
