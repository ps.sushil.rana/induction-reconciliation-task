package com.progressoft.application.producer;

import com.progressoft.application.writer.FileMatchingTransactionWriter;
import com.progressoft.application.writer.FileMissedTransactionWriter;
import com.progressoft.domain.TransactionOutput;

public class FileProducer implements TransactionProducer {

    private static final String MATCHING = "matching-transactions.csv";
    private static final String MISMATCHED = "mismatched-transactions.csv";
    private static final String MISSING = "missing-transactions.csv";
    private final FileMissedTransactionWriter fileMisMatchTransactionWriter;
    private final FileMatchingTransactionWriter fileMatchedTransactionWriter;
    private final FileMissedTransactionWriter fileMissedTransactionWriter;


    public FileProducer(FileMissedTransactionWriter fileMissedTransactionWriter,
                        FileMissedTransactionWriter fileMisMatchTransactionWriter,
                        FileMatchingTransactionWriter fileMatchingTransactionWriter) {
        this.fileMissedTransactionWriter = fileMissedTransactionWriter;
        this.fileMisMatchTransactionWriter = fileMisMatchTransactionWriter;
        this.fileMatchedTransactionWriter = fileMatchingTransactionWriter;

    }

    @Override
    public String produce(String path, TransactionOutput output) {
        fileMissedTransactionWriter
                .writeTransactions(path + MISSING, output.getMissingTransactions());

        fileMisMatchTransactionWriter
                .writeTransactions(path + MISMATCHED, output.getMisMatchTransactions());

        fileMatchedTransactionWriter
                .writeTransactions(path + MATCHING, output.getMatchingTransactions());

        return "Reconciliation finished.\nResult files are available in director ./reconciliation-results";
    }

}
