package com.progressoft.application.producer;

import com.progressoft.application.writer.FileMatchingTransactionWriter;
import com.progressoft.application.writer.FileMissedTransactionWriter;
import com.progressoft.domain.TransactionOutput;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class FileProducerTest {

    private static final String path = "output" + File.separator;

    @Test
    @DisplayName("Check if written to file")
    void whenGivenPath_thenShouldWriteToFile() {

        FileMissedTransactionWriter missingWriter = mock(FileMissedTransactionWriter.class);
        FileMissedTransactionWriter misMatchWriter = mock(FileMissedTransactionWriter.class);
        FileMatchingTransactionWriter matchingWriter = mock(FileMatchingTransactionWriter.class);


        TransactionOutput output = new TransactionOutput();

        FileProducer fileProducer = new FileProducer(missingWriter, misMatchWriter, matchingWriter);
        fileProducer.produce(path, output);

        verify(missingWriter).writeTransactions("output/missing-transactions.csv", output.getMissingTransactions());
        verify(misMatchWriter).writeTransactions("output/mismatched-transactions.csv", output.getMisMatchTransactions());
        verify(matchingWriter).writeTransactions("output/matching-transactions.csv", output.getMatchingTransactions());
    }

    @Test
    @DisplayName("Check if returns string")
    void whenWrittenToFile_thenShouldReturnString() {
        FileMissedTransactionWriter missedWriter = new FileMissedTransactionWriter();
        FileMissedTransactionWriter missingWriter = new FileMissedTransactionWriter();
        FileMatchingTransactionWriter matchingWriter = new FileMatchingTransactionWriter();

        FileProducer fileProducer=new FileProducer(missingWriter,missedWriter,matchingWriter);
        String actual=fileProducer.produce(path,new TransactionOutput());
        String expected= "Reconciliation finished.\nResult files are available in director ./reconciliation-results";
        Assertions.assertEquals(expected,actual);

    }

}
