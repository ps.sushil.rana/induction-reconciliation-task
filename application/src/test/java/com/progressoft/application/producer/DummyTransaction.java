package com.progressoft.application.producer;

import com.progressoft.domain.Transaction;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

public class DummyTransaction {

    public static Map<String, Transaction> createDummyCsvTransaction() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        Map<String, Transaction> transactionsCsvMap = new HashMap<>();
        Transaction transaction1 = new Transaction();
        transaction1.setId("TR-47884222201");
        transaction1.setDescription("online transfer");
        transaction1.setAmount(Double.parseDouble("140"));
        transaction1.setCurrencyCode(Currency.getInstance("USD"));
        transaction1.setPurpose("donation");
        transaction1.setTransactionDate(LocalDate.parse("2020-01-20", formatter));
        transaction1.setType("D");
        transactionsCsvMap.put("TR-47884222201", transaction1);

        Transaction transaction2 = new Transaction();
        transaction2.setId("TR-47884222202");
        transaction2.setDescription("atm withdrwal");
        transaction2.setAmount(Double.parseDouble("20"));
        transaction2.setCurrencyCode(Currency.getInstance("JOD"));
        transaction2.setPurpose("");
        transaction2.setTransactionDate(LocalDate.parse("2020-01-22", formatter));
        transaction2.setType("D");
        transactionsCsvMap.put("TR-47884222202", transaction2);

        Transaction transaction3 = new Transaction();
        transaction3.setId("TR-47884222203");
        transaction3.setDescription("counter withdrawal");
        transaction3.setAmount(Double.parseDouble("5000.0"));
        transaction3.setCurrencyCode(Currency.getInstance("JOD"));
        transaction3.setPurpose("");
        transaction3.setTransactionDate(LocalDate.parse("2020-01-25", formatter));
        transaction3.setType("C");
        transactionsCsvMap.put("TR-47884222203", transaction3);

        Transaction transaction4 = new Transaction();
        transaction4.setId("TR-47884222204");
        transaction4.setDescription("salary");
        transaction4.setAmount(Double.parseDouble("1200"));
        transaction4.setCurrencyCode(Currency.getInstance("JOD"));
        transaction4.setPurpose("donation");
        transaction4.setTransactionDate(LocalDate.parse("2020-01-31", formatter));
        transaction4.setType("D");
        transactionsCsvMap.put("TR-47884222204", transaction4);

        Transaction transaction5 = new Transaction();
        transaction5.setId("TR-47884222205");
        transaction5.setDescription("atm withdrwal");
        transaction5.setAmount(Double.parseDouble("60"));
        transaction5.setCurrencyCode(Currency.getInstance("JOD"));
        transaction5.setPurpose("");
        transaction5.setTransactionDate(LocalDate.parse("2020-02-02", formatter));
        transaction5.setType("D");
        transactionsCsvMap.put("TR-47884222205", transaction5);

        Transaction transaction6 = new Transaction();
        transaction6.setId("TR-47884222206");
        transaction6.setDescription("atm withdrwal");
        transaction6.setAmount(Double.parseDouble("500"));
        transaction6.setCurrencyCode(Currency.getInstance("USD"));
        transaction6.setPurpose("");
        transaction6.setTransactionDate(LocalDate.parse("2020-02-10", formatter));
        transaction6.setType("D");
        transactionsCsvMap.put("TR-47884222206", transaction6);
        return transactionsCsvMap;
    }
    public static Map<String, Transaction> createDummyJsonTransaction() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        Map<String, Transaction> transactionJsonMap = new HashMap<>();
        Transaction transaction1 = new Transaction();
        transaction1.setId("TR-47884222201");
        transaction1.setAmount(Double.parseDouble("140"));
        transaction1.setCurrencyCode(Currency.getInstance("USD"));
        transaction1.setPurpose("donation");
        transaction1.setTransactionDate(LocalDate.parse("20/01/2020", formatter));
        transactionJsonMap.put("TR-47884222201", transaction1);

        Transaction transaction2 = new Transaction();
        transaction2.setId("TR-47884222202");
        transaction2.setAmount(Double.parseDouble("30"));
        transaction2.setCurrencyCode(Currency.getInstance("JOD"));
        transaction2.setPurpose("donation");
        transaction2.setTransactionDate(LocalDate.parse("22/01/2020", formatter));
        transactionJsonMap.put("TR-47884222202", transaction2);

        Transaction transaction3 = new Transaction();
        transaction3.setId("TR-47884222205");
        transaction3.setAmount(Double.parseDouble("60"));
        transaction3.setCurrencyCode(Currency.getInstance("JOD"));
        transaction3.setPurpose("");
        transaction3.setTransactionDate(LocalDate.parse("03/02/2020", formatter));
        transactionJsonMap.put("TR-47884222205", transaction3);

        Transaction transaction4 = new Transaction();
        transaction4.setId("TR-47884222206");
        transaction4.setAmount(Double.parseDouble("500"));
        transaction4.setCurrencyCode(Currency.getInstance("USD"));
        transaction4.setPurpose("general");
        transaction4.setTransactionDate(LocalDate.parse("10/02/2020", formatter));
        transactionJsonMap.put("TR-47884222206", transaction4);

        Transaction transaction5 = new Transaction();
        transaction5.setId("TR-47884222217");
        transaction5.setAmount(Double.parseDouble("12000"));
        transaction5.setCurrencyCode(Currency.getInstance("JOD"));
        transaction5.setPurpose("salary");
        transaction5.setTransactionDate(LocalDate.parse("14/02/2020", formatter));
        transactionJsonMap.put("TR-47884222217", transaction5);


        Transaction transaction6 = new Transaction();
        transaction6.setId("TR-47884222203");
        transaction6.setAmount(Double.parseDouble("5000.0"));
        transaction6.setCurrencyCode(Currency.getInstance("JOD"));
        transaction6.setPurpose("not specified");
        transaction6.setTransactionDate(LocalDate.parse("25/01/2020", formatter));
        transactionJsonMap.put("TR-47884222203", transaction6);

        Transaction transaction7 = new Transaction();
        transaction7.setId("TR-47884222245");
        transaction7.setAmount(Double.parseDouble("420"));
        transaction7.setCurrencyCode(Currency.getInstance("USD"));
        transaction7.setPurpose("loan");
        transaction7.setTransactionDate(LocalDate.parse("12/01/2020", formatter));
        transactionJsonMap.put("TR-47884222245", transaction7);


        return transactionJsonMap;
    }
}
