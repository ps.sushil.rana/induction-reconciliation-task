package com.progressoft.application.writer;

import com.progressoft.domain.Transaction;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FileMatchingTransactionWriterTest {

    private static final String writeFile = "output/test1.csv";
    private static final String expectedHeader = "transaction id,amount,currecny code,value date";
    private static final String expectedRow1 = "TR-47884222201,140.00,USD,2020-01-20";
    private static final String expectedRow2 = "TR-47884222202,20.000,JOD,2020-01-22";

    @Test
    @DisplayName("Test whether matched transaction are written to file")
     void whenGivenList_thenShouldBeWrittenToFile() throws IOException {


        List<Transaction> transactions = createDummyTransaction();

        FileMatchingTransactionWriter writer = new FileMatchingTransactionWriter();
        writer.writeTransactions(writeFile,transactions);

        BufferedReader br = new BufferedReader(new FileReader(writeFile));

        String actualHeader = br.readLine();
        assertEquals(expectedHeader, actualHeader);

        String actualRow1 = br.readLine();
        assertEquals(expectedRow1, actualRow1);

        String actualRow2 = br.readLine();
        assertEquals(expectedRow2, actualRow2);
    }

    private List<Transaction> createDummyTransaction() {
        List<Transaction> transactions = new ArrayList<>();
        Transaction transaction1 = new Transaction();
        transaction1.setId("TR-47884222201");
        transaction1.setDescription("online transfer");
        transaction1.setAmount(Double.parseDouble("140"));
        transaction1.setCurrencyCode(Currency.getInstance("USD"));
        transaction1.setPurpose("donation");
        transaction1.setTransactionDate(LocalDate.parse("2020-01-20"));
        transaction1.setType("D");

        Transaction transaction2 = new Transaction();
        transaction2.setId("TR-47884222202");
        transaction2.setDescription("atm withdrwal");
        transaction2.setAmount(Double.parseDouble("20.000"));
        transaction2.setCurrencyCode(Currency.getInstance("JOD"));
        transaction2.setPurpose("");
        transaction2.setTransactionDate(LocalDate.parse("2020-01-22"));
        transaction2.setType("D");

        transactions.add(transaction1);
        transactions.add(transaction2);
        return transactions;
    }

}