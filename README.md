## Reconciliation System
An application for checking whether two sets of records matches each other.

## Running the application
- Go to Path:induction-reconciliation-task/src/main/java/org.ps/MainApplication.java
-  Run the application
- Give the inputs available in Path:induction-reconciliation-task/input
- Output can be found in Path:induction-reconciliation-task/output

## Feedback
- set input/output directory path main application
- usage of project structure and .gitignore
- add source and target file name in output
- code duplication
- extract abstract class for file reader
- extract abstract class for file writer
- exception handling for csv and json
- packaging structure for reader and writer
- differentiate csv and json before processing transaction
- multi-module : application, process, domain
- use logger
- analyze and inspect code
- integrate sonar