package com.progressoft.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.progressoft.domain.DummyTransaction.createDummyCsvTransaction;
import static com.progressoft.domain.DummyTransaction.createDummyJsonTransaction;
import static java.lang.Double.parseDouble;
import static java.time.LocalDate.parse;
import static java.util.Currency.getInstance;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

class TransactionComparisonTest {

    @Test
    @DisplayName("List of matching transactions comparison")
    void whenGivenMap_thenShouldReturnMatchingList() {
        TransactionComparison transactionComparison = new TransactionComparison(
                "SOURCE",
                "TARGET",
                createDummyCsvTransaction(),
                createDummyJsonTransaction());
        List<Transaction> actual = transactionComparison.compareSourceAndTarget().getMatchingTransactions();

        assertThat(actual)
                .extracting(
                        Transaction::getId,
                        Transaction::getCurrencyCode,
                        Transaction::getTransactionDate
                )
                .containsExactlyInAnyOrder(
                        tuple("TR-47884222201", getInstance("USD"), parse("2020-01-20")),
                        tuple("TR-47884222203", getInstance("JOD"), parse("2020-01-25")),
                        tuple("TR-47884222206", getInstance("USD"), parse("2020-02-10"))
                );
    }

    @Test
    @DisplayName("List of mismatched transaction comparison")
     void whenGivenMap_thenShouldReturnMismatchedList() {
        TransactionComparison transactionComparison = new TransactionComparison(
                "SOURCE",
                "TARGET",
                createDummyCsvTransaction(),
                createDummyJsonTransaction());
        List<Transaction> actual = transactionComparison.compareSourceAndTarget().getMisMatchTransactions();

        assertThat(actual)
                .extracting(
                        Transaction::getLocation,
                        Transaction::getId,
                        Transaction::getAmount
                )
                .containsExactlyInAnyOrder(
                        tuple("SOURCE", "TR-47884222202", parseDouble("20.0")),
                        tuple("TARGET", "TR-47884222202", parseDouble("30.0")),
                        tuple("SOURCE", "TR-47884222205", parseDouble("60.0")),
                        tuple("TARGET", "TR-47884222205", parseDouble("60.0"))
                );
    }

    @Test
    @DisplayName("List of missing transaction comparison")
    void whenGivenMap_thenShouldReturnMissingList() {
        TransactionComparison transactionComparison = new TransactionComparison("SOURCE", "TARGET",
                createDummyCsvTransaction(),
                createDummyJsonTransaction());
        List<Transaction> actual = transactionComparison.compareSourceAndTarget().getMissingTransactions();
        assertThat(actual)
                .extracting(
                        Transaction::getId,
                        Transaction::getAmount
                )
                .containsExactlyInAnyOrder(
                        tuple("TR-47884222204", parseDouble("1200.0")),
                        tuple("TR-47884222217", parseDouble("12000.0")),
                        tuple("TR-47884222245", parseDouble("420.0"))
                );
    }

}