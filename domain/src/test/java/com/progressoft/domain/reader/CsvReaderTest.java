package com.progressoft.domain.reader;

import com.progressoft.domain.Transaction;
import com.progressoft.domain.exception.FileReaderException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static java.lang.Double.parseDouble;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

class CsvReaderTest {

    private static final String CSV_FILE = "../input/bank-transactions.csv";

    @Test
    @DisplayName("Map of transactions comparison from csv")
    void whenGivenFile_thenReturnTransaction() throws Exception {
        CsvReader csvReader = new CsvReader(CSV_FILE);
        Map<String, Transaction> expected = csvReader.getTransactions();
        assertThat(expected)
                .extractingFromEntries(
                        Map.Entry::getKey,
                        e -> e.getValue().getDescription(),
                        e -> e.getValue().getAmount(),
                        e -> e.getValue().getType()
                )
                .containsExactlyInAnyOrder(
                        tuple("TR-47884222201", "online transfer", parseDouble("140"), "D"),
                        tuple("TR-47884222202", "atm withdrwal", parseDouble("20.0000"), "D"),
                        tuple("TR-47884222203", "counter withdrawal", parseDouble("5000"), "D"),
                        tuple("TR-47884222204", "salary", parseDouble("1200.000"), "C"),
                        tuple("TR-47884222205", "atm withdrwal", parseDouble("60.0"), "D"),
                        tuple("TR-47884222206", "atm withdrwal", parseDouble("500.0"), "D")
                );

    }

}

