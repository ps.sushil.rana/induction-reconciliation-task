package com.progressoft.domain.reader;

import com.progressoft.domain.exception.FileReaderException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

 class TransactionImporterTest {

    private static final String DUMMY_CSV_FILE = "input/bank-transactions.csv";
    private static final String DUMMY_TYPE = "xml";

    @Test
    @DisplayName("Invalid format test")
    void givenInvalidFileFormat_whenWriting_thenShouldThrowException() {
       FileReaderException ex = assertThrows(FileReaderException.class, () ->
                TransactionImporter.getFileReader(DUMMY_TYPE, DUMMY_CSV_FILE)
        );
        String expected = "Unrecognized file type" +DUMMY_TYPE ;
        Assertions.assertEquals(expected, ex.getMessage());

    }
}
