package com.progressoft.domain.reader;

import com.progressoft.domain.Transaction;
import com.progressoft.domain.exception.FileReaderException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Currency;
import java.util.Map;

import static java.lang.Double.parseDouble;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

class JsonReaderTest {

    private static final String JSON_FILE = "../input/online-banking-transactions.json";

    @Test
    @DisplayName("Map of transaction comparison from json")
    void whenGivenFile_thenShouldReturnTransaction() throws Exception {
        JsonReader jsonReader = new JsonReader(JSON_FILE);
        Map<String, Transaction> expected = jsonReader.getTransactions();
        assertThat(expected)
                .extractingFromEntries(
                        Map.Entry::getKey,
                        e -> e.getValue().getId(),
                        e -> e.getValue().getAmount(),
                        e -> e.getValue().getCurrencyCode()
                )
                .containsExactlyInAnyOrder(
                        tuple("TR-47884222201", "TR-47884222201", parseDouble("140"), Currency.getInstance("USD")),
                        tuple("TR-47884222205", "TR-47884222205", parseDouble("60"), Currency.getInstance("JOD")),
                        tuple("TR-47884222206", "TR-47884222206", parseDouble("500"), Currency.getInstance("USD")),
                        tuple("TR-47884222202", "TR-47884222202", parseDouble("30"), Currency.getInstance("JOD")),
                        tuple("TR-47884222217", "TR-47884222217", parseDouble("12000"), Currency.getInstance("JOD")),
                        tuple("TR-47884222203", "TR-47884222203", parseDouble("5000"), Currency.getInstance("JOD")),
                        tuple("TR-47884222245", "TR-47884222245", parseDouble("420"), Currency.getInstance("USD"))
                );
    }

}