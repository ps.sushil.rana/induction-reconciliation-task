package com.progressoft.domain.reader;

import com.progressoft.domain.Transaction;
import com.progressoft.domain.exception.FileReaderException;

import java.util.Map;

public class TransactionImporter {

    private static final String INPUT_PATH = "./";

    private TransactionImporter() {

    }

    public static Map<String, Transaction> readFile(String file, String fileFormat) throws FileReaderException {
        ReaderFile sourceReader = getFileReader(file, fileFormat);
        return sourceReader.getTransactions();
    }

    public static ReaderFile getFileReader(String type, String fileName) throws FileReaderException {

        String path = INPUT_PATH + fileName;
        if (type.equals("csv")) {
            return new CsvReader(path);
        }
        if (type.equals("json")) {
            return new JsonReader(path);
        }
        throw new FileReaderException("Unrecognized file type" +type);
    }

}
