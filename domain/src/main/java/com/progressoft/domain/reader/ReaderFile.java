package com.progressoft.domain.reader;


import com.progressoft.domain.Transaction;
import com.progressoft.domain.exception.FileReaderException;

import java.util.Map;

public interface ReaderFile {

    Map<String, Transaction> getTransactions() throws FileReaderException;

}
