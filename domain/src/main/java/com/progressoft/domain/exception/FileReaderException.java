package com.progressoft.domain.exception;

public class FileReaderException extends Exception {

    public FileReaderException(String message) {
        super(message);
    }
    public FileReaderException(String message,Throwable throwable){
        super(message,throwable);
    }
}
